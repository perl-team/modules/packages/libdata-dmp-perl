Source: libdata-dmp-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Lucas Kanashiro <kanashiro@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libregexp-stringify-perl <!nocheck>,
                     perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdata-dmp-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdata-dmp-perl.git
Homepage: https://metacpan.org/release/Data-Dmp
Rules-Requires-Root: no

Package: libdata-dmp-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libregexp-stringify-perl
Multi-Arch: foreign
Description: Perl module that dumps Perl data structures as Perl code
 Data::Dmp is a Perl dumper like Data::Dumper. It's compact (only about 150
 lines of code long), starts fast and does not use other module except
 Regexp::Stringify when dumping regexes. It produces compact output (similar
 to Data::Dumper::Concise). It roughly has the same speed as Data::Dumper
 (usually a bit faster for smaller structures), but does not offer the various
 formatting options. It supports dumping objects, regexes, circular
 structures, coderefs. Its code is based on Data::Dump.
